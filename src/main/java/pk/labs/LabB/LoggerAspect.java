package pk.labs.LabB;

import pk.labs.LabB.Contracts.Logger;

import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;



@Component("logowanie")
public class LoggerAspect{
	private Logger logowanie;
        
        public void wyjscieZMetody(JoinPoint joinPoint, Object result) {
		if(logowanie!=null)
			logowanie.logMethodExit(joinPoint.getSignature().getName(), result);
	}
        
        public void wejscieDoMetody(JoinPoint joinPoint) {
		if(logowanie!=null)
			logowanie.logMethodEntrance(joinPoint.getSignature().getName(), joinPoint.getArgs());
	}
        
        
	void setLogger(Logger logowanie)
	{
		this.logowanie =logowanie;
	}
	
	
}
