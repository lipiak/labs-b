package pk.labs.LabB.ui;

import java.awt.Color;

import javax.swing.JPanel;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.labs.LabB.Contracts.ControlPanel;
import pk.labs.LabB.Contracts.Display;

@Component("negative")
public class NegativeableImplementation implements Negativeable{
        
    //@Autowired
        private Utils utilities = new Utils();
	
        
        @Override
	public void negative() {
            //JPanel cp = ((ControlPanel) AopContext.currentProxy()).getPanel();
            //utilities.negateComponent(cp);
            
            //JPanel disp = ((Display) AopContext.currentProxy()).getPanel();
            //utilities.negateComponent(disp);
            Object o = AopContext.currentProxy();
            
            if(o instanceof ControlPanel)
            {
                utilities.negateComponent(((ControlPanel)o).getPanel());
            }
            
            if(o instanceof Display)
            {
                utilities.negateComponent(((Display)o).getPanel());
            }
            
            
	}
	
}
