package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.display.MyDisplay";
    public static String controlPanelImplClassName = "pk.labs.LabB.ControlPanel.MyControlPanel";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.IMain";
    public static String mainComponentImplClassName = "pk.labs.LabB.main.Main";
    public static String mainComponentBeanName = "main";
    // endregion

    // region P2
    public static String mainComponentMethodName = "Otworz";
    public static Object[] mainComponentMethodExampleParams = new Object[] {"otwieram" };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "logowanie";
    // endregion
}
